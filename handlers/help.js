const { Composer } = require('telegraf')
const auth = require('../middleware/auth')

const start = `
Добро пожаловать!
Это бот взаимодействет с порталом ${process.env.BITRIX24_URL}

/help - список доступных команд
`

const help = `
/login - авторизация
/logout - отмена авторизации
/search <code>...</code> - поиск контакта
`

const extra = {
  parse_mode: `HTML`,
}

module.exports = (bot) => {
  bot.start(Composer.privateChat(Composer.reply(start, extra)))
  bot.help(Composer.privateChat(auth, Composer.reply(help, extra)))
}
