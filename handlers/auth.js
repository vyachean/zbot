const { button } = require('../helpers')
const Auth = require('../modules/auth')
const auth = require('../middleware/auth')

const { Composer } = require('telegraf')
const messageOptions = {
  parse_mode: `HTML`,
}
const HANDLER_NAME = 'auth'

function login (ctx) {
  ctx.reply(`
Привет, ${ctx.from.first_name}.
Для авторизации бота с порталом bitrix24, необходимо пройти по ссылке и авторизоваться в форме или создать вебхук.
  `, {
    ...messageOptions,
    reply_markup:
      {
        inline_keyboard: [
          [
            button({
              text: 'Как получить вебхук',
              handler: HANDLER_NAME,
              data: 'guide.for.webhook',
            }),
          ],
        ],
      },
  })
}

function logout (ctx) {
  ctx.reply(`
Вы хотите удалить все ваши данные из бота?
  `, {
    ...messageOptions,
    reply_markup: {
      inline_keyboard: [
        [
          button({
            text: 'Да, удалить 🗑',
            handler: HANDLER_NAME,
            data: 'confirm.unregister',
          }),
          button({
            text: 'Нет, передумал',
            handler: HANDLER_NAME,
            data: 'cancel.unregister',
          }),
        ],
        [
          button({
            text: 'Какие данные? 🧐',
            handler: HANDLER_NAME,
            data: 'get.personal',
          }),
        ],
      ],
    },
  })
}

async function actions (ctx) {
  if (ctx.callbackQuery) {
    const data = ctx.callbackQuery.data.split(':')[1]
    switch (data) {
      case 'guide.for.webhook':
        ctx.reply(`
Зайдите на портал по адресу ${process.env.BITRIX24_URL}/marketplace/hook/
Нажмите на кнопку "Добавить вебхук", затем "Входящий вебхук"
Выбери права "Пользователи (user)" и сохраните
После сохранения в списке вебхуков найдите созданный вебхук
Нажмите "Редактировать"
Пришлите пример URL для вызова REST

⚠️ Данный код является конфиденциальной информацией. Его необходимо держать в секрете.
        `, messageOptions)
        break
      case 'confirm.unregister':
        await Auth.unregister(ctx.db, { telegramID: ctx.from.id })
        await ctx.editMessageText('Данные удалены', messageOptions)
        break
      case 'cancel.unregister':
        await ctx.deleteMessage()
        break
      case 'get.personal':
        await ctx.editMessageText(`${
            JSON.stringify(
              await Auth.getMyProfile(ctx.db, { telegramID: ctx.from.id }),
            )
          }`,
          messageOptions,
        )
        break
      default:
        break
    }
  }
}

function registerWebHook (ctx) {
  const { message, from } = ctx
  const [bitrixID, tokenWebHook] = message.text.replace(
    `${process.env.BITRIX24_URL}/rest/`, '').
    split('/')
  Auth.register(ctx.db, { bitrixID, tokenWebHook, telegramID: from.id }).
    then((user) => {
      ctx.deleteMessage()
      ctx.reply(`
Привет, ${user.name}!
Добро пожаловать 🙂
    `, messageOptions)
    }).catch(e => {
    ctx.reply(`${e.message}`, messageOptions)
  })
}

module.exports = (bot) => {
  bot.command('test', (ctx) => {
    console.log('start test')
    ctx.reply('start test')
    for (let i = 0; i < 200; i += 1) {
      ctx.reply(`${ctx.from.name} ${i}`)
    }
  })
  bot.command('logout', Composer.privateChat(auth, logout))
  bot.command('login', Composer.privateChat(login))
  bot.action(new RegExp(`^${HANDLER_NAME}`), Composer.privateChat(actions))
  bot.hears(new RegExp(
    `^${process.env.BITRIX24_URL}/rest/\\d+/[0-9a-z]+/`),
    Composer.privateChat(registerWebHook))
}