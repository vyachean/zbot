const { Composer } = require('telegraf')
const auth = require('../middleware/auth')
const searchUser = require('../modules/searchUser')
const { button } = require('../helpers')
const HANDLER_NAME = 'search'

async function search (ctx) {
  const query = ctx.message.text.replace('/search', '').trim()
  const list = await searchUser.search({ query, auth: ctx.auth, db: ctx.db })
  const keyboard = list.slice(0, 9).
    map(({ name, lastName, secondName, bitrixID }) => ([
      button({
        handler: HANDLER_NAME,
        text: `${lastName} ${name} ${secondName}`,
        data: `get.user.${bitrixID}`,
      })]))
  ctx.reply(`
 Найдено ${list.length} сотрудников${list.length > 10 || !list.length
    ? ', уточните запрос'
    : ''}
  `,
    {
      parse_mode: 'HTML',
      reply_markup: {
        inline_keyboard: [
          ...keyboard,
        ],
      },
    },
  )
}

async function action (ctx) {
  if (ctx.callbackQuery) {
    const data = ctx.callbackQuery.data.split(':')[1]

    if (/^get\.user\.[0-9]+/.test(data)) {
      const user = await searchUser.getUser(
        { auth: ctx.auth, db: ctx.db, id: data.split('.')[2] })
      await ctx.replyWithPhoto(user.photo, {
        caption: `
<b>${user.lastName} ${user.name} ${user.secondName}</b>
${user.city}, ${user.workCompany}, ${user.workPosition}
День рождения ${(new Date(user.birthday)).toLocaleString('ru',
          { day: 'numeric', month: 'long', year: 'numeric' })}
`,
        parse_mode: 'HTML',
      })
      await ctx.replyWithContact(user.mobile, user.name,
        { last_name: user.lastName })
    }
  }
}

module.exports = (bot) => {
  bot.command('search', Composer.privateChat(auth, search))
  bot.hears(/[а-яёА-ЯЁ]{3,}/, Composer.privateChat(auth, search))
  bot.action(new RegExp(`^${HANDLER_NAME}`), Composer.privateChat(auth, action))
}
