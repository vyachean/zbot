const handlers = [
  'help',
  'auth',
  'search',
]

module.exports = (bot) => handlers.forEach(
  handler => require(`./${handler}`)(bot))