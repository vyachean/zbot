require(`dotenv`).config()

const { Telegraf } = require('telegraf')
const { MongoClient } = require('mongodb')
const express = require('express')
const callQueue = require('./middleware/callQueue')
const expressApp = express()

const config = {
  TG_TOKEN,
  TG_HOOK,
  TG_DOMAIN,
  MONGODB_URI,
} = process.env

config.PORT = process.env.PORT || 8080

async function startBot () {
  const bot = new Telegraf(config.TG_TOKEN)

  bot.catch(console.error)

  await bot.telegram.deleteWebhook()

  expressApp.get('/', (req, res) => {
    res.send('Hello World!')
  })

  bot.context.db = (await MongoClient.connect(config.MONGODB_URI,
    { useNewUrlParser: true })).db()

  bot.use(callQueue)
  require('./handlers')(bot)

  if (config.TG_HOOK && config.TG_DOMAIN) {
    expressApp.use(bot.webhookCallback(config.TG_HOOK))
    await bot.telegram.setWebhook(
      `${config.TG_DOMAIN}${config.TG_HOOK}`)
  } else {
    bot.startPolling()
  }

  expressApp.listen(config.PORT, () => {
    console.log(`Example app listening on port ${config.PORT}!`)
  })
}

startBot()