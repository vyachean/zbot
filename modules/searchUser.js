const Bitrix = require('@2bad/bitrix').default
const { Method } = require('@2bad/bitrix')

module.exports = {
  async search ({ auth, query }) {
    const findUsersBitrix = await Bitrix(
      `${process.env.BITRIX24_URL}/rest/${auth.bitrixID}/${auth.tokenWebHook}`).
      call('user.search', { FILTER: { FIND: query } })
    return findUsersBitrix.result.filter(({ ACTIVE }) => ACTIVE).
      map(bitrixProfile => ({
        bitrixID: bitrixProfile.ID,
        name: bitrixProfile.NAME,
        lastName: bitrixProfile.LAST_NAME,
        secondName: bitrixProfile.SECOND_NAME,
        mobile: bitrixProfile.PERSONAL_MOBILE,
      }))
  },

  async getUser ({ auth, id }) {
    const { result: [user] } = await Bitrix(
      `${process.env.BITRIX24_URL}/rest/${auth.bitrixID}/${auth.tokenWebHook}`).
      users.get(id)
    if (user.ACTIVE) {
      return {
        bitrixID: user.ID,
        name: user.NAME,
        lastName: user.LAST_NAME,
        secondName: user.SECOND_NAME,
        mobile: user.PERSONAL_MOBILE,
        photo: user.PERSONAL_PHOTO,
        workCompany: user.WORK_COMPANY,
        workPosition: user.WORK_POSITION,
        city: user.PERSONAL_CITY,
        birthday: user.PERSONAL_BIRTHDAY,
      }
    } else {
      throw { message: 'Пользователь не найден' }
    }
  },
}