const Bitrix = require('@2bad/bitrix').default
const { Method } = require('@2bad/bitrix')

module.exports = {
  async register (db, { bitrixID, tokenWebHook, telegramID }) {
    if (tokenWebHook && bitrixID) {
      const bitrix = Bitrix(
        `${process.env.BITRIX24_URL}/rest/${bitrixID}/${tokenWebHook}`)

      const { result } = await bitrix.users.get(bitrixID)
      const users = db.collection('users')
      if (result.length === 1 && result[0].ACTIVE) {
        const profile = result[0]

        const newUser = {
          bitrixID,
          tokenWebHook,
          telegramID,
          name: profile.NAME,
          lastName: profile.LAST_NAME,
          secondName: profile.SECOND_NAME,
          mobile: profile.PERSONAL_MOBILE,
          lastUpdateTime: (new Date()).toISOString(),
        }
        const { value } = await users.findOneAndReplace({ bitrixID }, newUser)
        if (!value) {
          await users.insertOne(newUser)
          this.startIndex()
        }
        return newUser
      } else {
        throw {
          message: `Не удалось найти пользователя ${bitrixID} на портале`,
        }
      }
    }
  },
  async isAuth (db, { telegramID }) {
    const users = db.collection('users')
    const user = await users.findOne({ telegramID })
    if (user && user.tokenWebHook) {
      const lastUpdateDate = new Date(user.lastUpdateTime)

      if (user.lastUpdateTime && Date.now() <=
        lastUpdateDate.setHours(lastUpdateDate.getHours() + 24)) {
        return {
          tokenWebHook: user.tokenWebHook,
          bitrixID: user.bitrixID,
        }
      }

      if (!user.lastUpdateTime
        || Date.now() >
        lastUpdateDate.setHours(lastUpdateDate.getHours() + 24)) {
        try {
          await this.updateUser(db, { user })
          return {
            tokenWebHook: user.tokenWebHook,
            bitrixID: user.bitrixID,
          }
        } catch (e) {
          return false
        }
      }
    }
    return false
  },
  async updateUser (db, { user }) {
    const bitrix = Bitrix(
      `${process.env.BITRIX24_URL}/rest/${user.bitrixID}/${user.tokenWebHook}`)
    const { result: [bitrixProfile] } = await bitrix.users.get(
      user.bitrixID)
    if (bitrixProfile.ACTIVE) {
      const users = db.collection('users')
      return await users.findOneAndUpdate(
        { bitrixID: user.bitrixID }, {
          bitrixID: user.bitrixID,
          tokenWebHook: user.tokenWebHook,
          telegramID: user.telegramID,
          name: bitrixProfile.NAME,
          lastName: bitrixProfile.LAST_NAME,
          secondName: bitrixProfile.SECOND_NAME,
          mobile: bitrixProfile.PERSONAL_MOBILE,
          lastUpdateTime: (new Date()).toISOString(),
        })
    } else {
      this.unregister(db, { telegramID: user.telegramID })
      throw {
        message: `Пользователь ${user.bitrixID} деактивирован на портале`,
      }
    }
  },
  async unregister (db, { telegramID }) {
    const users = db.collection('users')
    return await users.findOneAndDelete({ telegramID })
  },
  async getMyProfile (db, { telegramID }) {
    const users = db.collection('users')
    const user = await users.findOne({ telegramID })
    return { ...user, _id: undefined, lastUpdateTime: undefined }
  },
  async startIndex (db) {
    const users = db.collection('users')
    const indexes = await users.indexes()
    if (!indexes.find(index => (index.name === 'indexNames'))) {
      users.createIndex({
          name: 'text',
          lastName: 'text',
          secondName: 'text',
        },
        {
          name: 'indexNames',
          default_language: 'russian',
        })
    }
  },
}