const Auth = require('../modules/auth')

module.exports = async (ctx, next) => {
  ctx.auth = await Auth.isAuth(ctx.db, { telegramID: ctx.from.id })

  if (ctx.auth) {
    next()
  } else {
    ctx.reply(`
Для этого действия пройдите авторизацию
/login
    `)
  }
}