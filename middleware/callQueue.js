const callStacks = {}

class CallStacks {
  constructor ({ timeout, stacks }) {
    this.stacks = [...stacks]
    this.active = false
    this.timeout = timeout
    this.start()
  }

  push (fn) {
    this.stacks.push(fn)
  }

  get length () {
    return this.stacks.length
  }

  async start () {
    this.active = true
    while (this.length > 0) {
      try {
        await this.stacks[0]()
        this.stacks.splice(0, 1)
        await new Promise(res => setTimeout(res, 500))
      } catch (e) {
        if (e.code === 429) {
          await new Promise(res => setTimeout(res, this.timeout * 1000))
        } else {
          throw e
        }
      }
    }
    this.active = false
  }
}

module.exports = async (ctx, next) => {
  const originalReply = ctx.reply.bind(ctx)
  ctx.tg.options.webhookReply = false
  ctx.reply = function () {
    return new Promise((res) => {
      if (callStacks[this.chat.id] &&
        callStacks[this.chat.id].active) {
        callStacks[this.chat.id].push(
          () => originalReply(...arguments).then(res))
      } else {
        callStacks[this.chat.id] = new CallStacks(
          {
            timeout: 30,
            stacks: [() => originalReply(...arguments).then(res)],
          })
      }
    })
  }
  next()
}