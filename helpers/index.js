function button ({ text, data, handler, url, login_url }) {
  const callback_data = handler && `${handler}:${data}`;
  return {
    text,
    callback_data,
    url,
    login_url,
  }
}

module.exports = {
  button,
}